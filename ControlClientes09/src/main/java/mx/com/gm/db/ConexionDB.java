package mx.com.gm.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionDB {
	
	 	private static final String CONTROLADOR = "com.mysql.jdbc.Driver";
	    private static final String URL = "jdbc:mysql://localhost:3306/test2?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	    private static final String USUARIO = "root";
	    private static final String CLAVE = "admin";

	    public Connection conectar() {
	        Connection conexion = null;
	        
	        try {
	            Class.forName(CONTROLADOR);
	            conexion = DriverManager.getConnection(URL, USUARIO, CLAVE);
	            System.out.println("Conexión OK");

	        } catch (ClassNotFoundException e) {
	            System.out.println("Error al cargar el controlador");
	            e.printStackTrace();

	        } catch (SQLException e) {
	            System.out.println("Error en la conexión");
	            System.out.println(e);
	            e.printStackTrace();
	        }
	        
	        return conexion;
	    }
}
