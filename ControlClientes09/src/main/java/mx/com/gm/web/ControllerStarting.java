package mx.com.gm.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
// Library for sharing information between controller and UI
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/*Library for viewing log */
import lombok.extern.slf4j.Slf4j;
import mx.com.gm.domain.Persona;
import mx.com.gm.servicio.PersonaService;

@Controller
// Library for logging 
@Slf4j
public class ControllerStarting {
	@Autowired
	private PersonaService personaService;
	
	@GetMapping("/")
	public String inicio(Model model, @AuthenticationPrincipal User user) {
		var personas = personaService.listarPersona();
		log.info("Usuario Logeado:  "  + user);
		log.info("Ejecutando el controlador Spring MVC");
		model.addAttribute("personas", personas);
		var saldoTotal= 0D;
		for (var p: personas){
			saldoTotal += p.getSaldo();
		}
		model.addAttribute("saldoTotal", saldoTotal);
		model.addAttribute("totalClientes", personas.size());
		return "index";
	}
	
	@GetMapping("/agregar")
	public String agregar(Persona persona) {
		return "modificar";
	}
	
	@PostMapping("/guardar")
	public String guardar(@Valid Persona persona, Errors errores) {
		if(errores.hasErrors()) {
			log.info(errores.toString());
			return "modificar";
		}
		personaService.guardar(persona);
		return "redirect:/";
	}
	
	@GetMapping("/editar/{idPersona}")
	public String editar(Persona persona, Model modelo){
		persona = personaService.encontrarPersona(persona);
		modelo.addAttribute("persona", persona);
		return "modificar";
	}
	
	//@GetMapping("/eliminar/{idPersona}")
	@GetMapping("/eliminar")
	public String eliminar(Persona persona) {
		personaService.eliminar(persona);
		return "redirect:/";
	}
	

}
