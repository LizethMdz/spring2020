package mx.com.gm.domain;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

// @Data Automaticamente crea los getters y settters
// @Entity Declara la clase como entidad
// @Table Definimos a que tabla hacemos referencia en la DB
// @Id Define el uso de un ID
// @GeneretedValue Define el uso de un ID como primary key
// @NotEmpty - Restringimos que el campo no sea vacio y que haya cadena

@Data
@Entity
@Table(name="persona")
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPersona;
	
	@NotEmpty
	private String nombre;
	
	@NotEmpty
	private String apellido;
	
	@NotEmpty
	@Email
	private String email;
	
	private String telefono;

}
