package mx.com.gm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import mx.com.gm.db.ConexionDB;

@SpringBootApplication
public class HolaSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(HolaSpringApplication.class, args);
		
		//Conexion a la Basede Datos
		var bd = new ConexionDB(); 
		bd.conectar();
	}

}
