package mx.com.gm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
// Library for sharing information between controller and UI
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
/*Library for viewing log */
import lombok.extern.slf4j.Slf4j;
import mx.com.gm.domain.Persona;

@Controller
// Library for logging 
@Slf4j
public class ControllerStarting {
	
	// Variable from application.properties
	@Value("${index.saludo}")
	private String _saludo;
	
	
	@GetMapping("/")
	public String inicio(Model model) {
		var  _mensaje= "Hola con Thymeleaf";
		var persona = new Persona();
		persona.setNombre("Luis");
		persona.setApellido("Benitez");
		persona.setEmail("luis@gmail.com");
		persona.setTelefono("5465456");
		
		var persona2 = new Persona();
		persona2.setNombre("Mago");
		persona2.setApellido("Aguilar");
		persona2.setEmail("maguilar@gmail.com");
		persona2.setTelefono("3415341");
		
		//Opcion 1
		//ArrayList
		//List<Persona> people = new ArrayList();
		//people.add(persona);
		//people.add(persona2);
		
		//Opcion 2
		var personas = Arrays.asList(persona, persona2);
		
		//Opcion 3
		//persona.addPeople(persona);
		//persona.addPeople(persona2);
		
		
		model.addAttribute("msm", _mensaje);
		model.addAttribute("saludo", _saludo);
		model.addAttribute("personas", personas);
		return "index";
	}
	

}
