package mx.com.gm.domain;

import lombok.Data;

// Automaticamente crea los getters y settters

@Data

public class Persona {
	
	private String nombre;
	private String apellido;
	private String email;
	private String telefono;

}
