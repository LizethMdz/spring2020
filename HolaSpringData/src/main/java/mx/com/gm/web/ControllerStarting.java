package mx.com.gm.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
// Library for sharing information between controller and UI
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
/*Library for viewing log */
import lombok.extern.slf4j.Slf4j;
import mx.com.gm.dao.IPersonaDao;
import mx.com.gm.domain.Persona;

@Controller
// Library for logging 
@Slf4j
public class ControllerStarting {
	@Autowired
	private IPersonaDao personaDao;
	

	@GetMapping("/")
	public String inicio(Model model) {
		var personas = personaDao.findAll();
		model.addAttribute("personas", personas);
		return "index";
	}
	

}
