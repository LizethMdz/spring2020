package mx.com.gm;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
/*Library for viewing log */
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class ControllerStarting {
	@GetMapping("/")
	public String inicio() {
		return "Hola Mundo con Spring";
	}

}
